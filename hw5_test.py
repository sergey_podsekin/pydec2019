from Homework5 import generate_number
from Homework5 import compare
import unittest


class Check_hw5(unittest.TestCase):

    def test_generate_len(self):
        assert len(generate_number()) == 4

    def test_generate(self):
        assert generate_number().isdigit()

    def test_compare(self):
        self.assertEqual(compare('1234', '1233'), 3)


if __name__ == '__main__':
    unittest.main()

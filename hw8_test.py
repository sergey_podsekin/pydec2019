from hw8 import sharp_remove
import unittest


class Check_hw8(unittest.TestCase):

    def test_sharp(self):
        self.assertEqual(sharp_remove(['a', '#', 'b', 'c']),['b', 'c'])

    @unittest.expectedFailure
    def test_incorrect_len_answer(self):
        self.assertEqual(sharp_remove(['a', 'b', 'c']),['b', 'c'])

    def test_sharps_only(self):
        self.assertEqual(sharp_remove(['#', '#', '#']), ['#', '#'])


if __name__ == '__main__':
    unittest.main()

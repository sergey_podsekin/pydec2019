def sharp_remove(x):
    sh = x.index('#')
    if sh == 0:
        del x[sh]
    else:
        del x[sh], x[sh - 1]
    return x


def game():
    sharp = list(input('Введите строку\n'))
    if '#' in sharp:
        while '#' in sharp:
            lis = sharp_remove(sharp)
        print(f"Обработанная строка: {''.join(lis)}")
    else:
        print(''.join(sharp))

x = list("###")
print(x)
print(sharp_remove(['#', '#', '#']))
# Быки и Коровы

import random

print('\nДобро пожаловать в игру Быки и Коровы!\n')


# Функция генерации случайного числа
def generate_number():
    arr = []
    while len(arr) != 4:
        x = random.randint(1, 9)
        if str(x) not in arr:
            arr.append(str(x))
    num = ''.join(arr)
    print(num)
    return num


# функция ввода числа
def guess_func():
    guess = input('Пожалуйста введите 4-хзначное число\n')
    if not guess.isdigit():
        print('Вы ввели не число!')
        return 0
    if len(str(guess)) != 4:
        print('Вы ввели не 4-хзначное число!')
        return 0
    if len(guess) != len(set(guess)):
        print('Цифры не должны повторяться!')
        return 0
    return guess


# функция сравнения чисел
def compare(guess):
    # Если число угадано
    if x == number:
        return 1
    # Считаем коров
    cows = 0
    for i in x:
        if i in number:
            cows += 1
    # Считаем быков
    bulls = 0
    for i in range(4):
        if x[i] == number[i]:
            bulls += 1
    return print('Вы не угадали число\n Количество коров: {}\n '
                 'Количество быков: {}'.format(cows - bulls, bulls))


# Цикл работы программы
number = generate_number()
result = False
while not result:
    x = 0
    while x == 0:
        x = guess_func()
    result = compare(x)
print('Поздравляю! Вы угадали число!')
